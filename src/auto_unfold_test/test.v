// +FHDR------------------------------------------------------------
//                 Copyright (c) 2023 NOVAUTO.
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : test.v
// Author        : ICer
// Created On    : 2023-11-21 11:22
// Last Modified : 2023-11-21 17:52 by ICer
// -----------------------------------------------------------------
// Description:
//
//
// -FHDR------------------------------------------------------------


module test #(
    //parameter
)( /*AUTOARG*/
   // Inputs
   clk, rst_n
   );

// ----------------------------------------------------------------
// Interface declare
// ----------------------------------------------------------------
input clk;
input rst_n;

// ----------------------------------------------------------------
// Wire declare
// ----------------------------------------------------------------

// ----------------------------------------------------------------
// AUTO declare
// ----------------------------------------------------------------
/*AUTOOUTPUT*/
/*AUTOINPUT*/
/*AUTOWIRE*/

/* AUTO_UNFOLD
#for i 1..4
wire [15:0] sig#i#;
END */
//AUTO_UNFOLD_START
wire [15:0] sig1;
wire [15:0] sig2;
wire [15:0] sig3;
wire [15:0] sig4;
//AUTO_UNFOLD_END

/* AUTO_UNFOLD
#for i 1..1
wire [15:0] sig#i#;
END */
//AUTO_UNFOLD_START
wire [15:0] sig1;
//AUTO_UNFOLD_END

/* AUTO_UNFOLD
#for i 5..1
wire [15:0] sig#i#;
END */
//AUTO_UNFOLD_START
wire [15:0] sig5;
wire [15:0] sig4;
wire [15:0] sig3;
wire [15:0] sig2;
wire [15:0] sig1;
//AUTO_UNFOLD_END

/* AUTO_UNFOLD
#for i 5..1
wire [15:0] sig#i#;
wire [15:0] reg#i#;

END */
//AUTO_UNFOLD_START
wire [15:0] sig5;
wire [15:0] reg5;

wire [15:0] sig4;
wire [15:0] reg4;

wire [15:0] sig3;
wire [15:0] reg3;

wire [15:0] sig2;
wire [15:0] reg2;

wire [15:0] sig1;
wire [15:0] reg1;

//AUTO_UNFOLD_END

/* AUTO_UNFOLD
#for i 2..1
#for j 5..5
#for k 4..3
wire [15:0] k#k#_sig#j#_sub#i#;
END */
//AUTO_UNFOLD_START
wire [15:0] k4_sig5_sub2;
wire [15:0] k4_sig5_sub1;
wire [15:0] k3_sig5_sub2;
wire [15:0] k3_sig5_sub1;
//AUTO_UNFOLD_END

localparam LOOP_I_MIN = 3, LOOP_I_MAX = 5;
localparam LOOP_J_MIN = 2;

/* AUTO_UNFOLD
#for i LOOP_I_MIN..LOOP_I_MAX
#for j LOOP_J_MIN..5
wire [15:0] sig#j#_sub#i#;
END */
//AUTO_UNFOLD_START
wire [15:0] sig2_sub3;
wire [15:0] sig2_sub4;
wire [15:0] sig2_sub5;
wire [15:0] sig3_sub3;
wire [15:0] sig3_sub4;
wire [15:0] sig3_sub5;
wire [15:0] sig4_sub3;
wire [15:0] sig4_sub4;
wire [15:0] sig4_sub5;
wire [15:0] sig5_sub3;
wire [15:0] sig5_sub4;
wire [15:0] sig5_sub5;
//AUTO_UNFOLD_END

/* AUTO_UNFOLD
#for i LOOP_I_MIN..LOOP_I_MAX
#for j LOOP_J_MIN..5
wire [15:0] sig#i%j#_#j#_sub#i#;
END */
//AUTO_UNFOLD_START
wire [15:0] sig1_2_sub3;
wire [15:0] sig0_2_sub4;
wire [15:0] sig1_2_sub5;
wire [15:0] sig0_3_sub3;
wire [15:0] sig1_3_sub4;
wire [15:0] sig2_3_sub5;
wire [15:0] sig3_4_sub3;
wire [15:0] sig0_4_sub4;
wire [15:0] sig1_4_sub5;
wire [15:0] sig3_5_sub3;
wire [15:0] sig4_5_sub4;
wire [15:0] sig0_5_sub5;
//AUTO_UNFOLD_END

/* AUTO_UNFOLD
#for i 3..2
#for j 0..0
#for k 5..4
IF(#i==3 and k==4#)reg [15:0] sig_k#k#_j#j#_i#i#;
END */
//AUTO_UNFOLD_START
reg [15:0] sig_k4_j0_i3;
//AUTO_UNFOLD_END

localparam SEL_NUM = 32;
localparam SEL_NUM_SUB1 = 31;
/* AUTO_UNFOLD
#for i 0..SEL_NUM_SUB1
IF(#i==0#)assign out_sig = (sel == 3'd#i#) ? in_sig#i# :
IF(#i>0 and i<SEL_NUM_SUB1#)                 (sel == 3'd#i#) ? in_sig#i# :
IF(#i==SEL_NUM_SUB1#)                                  in_sig#i# ;
END */
//AUTO_UNFOLD_START
assign out_sig = (sel == 3'd0) ? in_sig0 :
                 (sel == 3'd1) ? in_sig1 :
                 (sel == 3'd2) ? in_sig2 :
                 (sel == 3'd3) ? in_sig3 :
                 (sel == 3'd4) ? in_sig4 :
                 (sel == 3'd5) ? in_sig5 :
                 (sel == 3'd6) ? in_sig6 :
                 (sel == 3'd7) ? in_sig7 :
                 (sel == 3'd8) ? in_sig8 :
                 (sel == 3'd9) ? in_sig9 :
                 (sel == 3'd10) ? in_sig10 :
                 (sel == 3'd11) ? in_sig11 :
                 (sel == 3'd12) ? in_sig12 :
                 (sel == 3'd13) ? in_sig13 :
                 (sel == 3'd14) ? in_sig14 :
                 (sel == 3'd15) ? in_sig15 :
                 (sel == 3'd16) ? in_sig16 :
                 (sel == 3'd17) ? in_sig17 :
                 (sel == 3'd18) ? in_sig18 :
                 (sel == 3'd19) ? in_sig19 :
                 (sel == 3'd20) ? in_sig20 :
                 (sel == 3'd21) ? in_sig21 :
                 (sel == 3'd22) ? in_sig22 :
                 (sel == 3'd23) ? in_sig23 :
                 (sel == 3'd24) ? in_sig24 :
                 (sel == 3'd25) ? in_sig25 :
                 (sel == 3'd26) ? in_sig26 :
                 (sel == 3'd27) ? in_sig27 :
                 (sel == 3'd28) ? in_sig28 :
                 (sel == 3'd29) ? in_sig29 :
                 (sel == 3'd30) ? in_sig30 :
                                  in_sig31 ;
//AUTO_UNFOLD_END

endmodule
// Local Variables:
// verilog-auto-inst-param-value:t
// verilog-library-directories:(".")
// verilog-library-extensions:(".v")
// End:

